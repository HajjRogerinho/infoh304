#include "list.h"

Liste::Liste()
{
	nombreNoeuds = 0;
	tete = NULL;
}

Liste::~Liste()
{
	while (tete != NULL)
	{
		supprime();
	}
}

void Liste::insere(string chaine)
{
	Noeud* n = new Noeud(chaine);
	n->setSuivant(tete);
	tete = n;
	nombreNoeuds++;
}

void Liste::supprime()
{
	if (tete != NULL)
	{
		Noeud* temp = tete;
		tete = tete->getSuivant();
		delete(temp);
		nombreNoeuds--;
	}
	else
		cout << "La liste est deja vide !" << endl;
}

void Liste::imprimeListe() const
{
	Noeud* temp = tete;
	cout << "Liste avec " << nombreNoeuds << " elements :" << endl;
	while (temp != NULL)
	{
		cout << "Mot : " << temp->getData() << endl;
		temp = temp->getSuivant();
	}
}

int Liste::getNombreNoeuds() const
{
	return nombreNoeuds;
}

Noeud* Liste::getTete()
{
	return tete;
}
