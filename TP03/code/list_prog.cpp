#include <iostream>
#include <fstream>
#include "list.h"
using std::cout;
using std::endl;
using std::ifstream;

int main(int argc, char *argv[])
{
	ifstream fichier (argv[1]);
	if (fichier.is_open())
	{
		Liste liste;
		string mot;
		while (fichier >> mot)
			liste.insere(mot);
		fichier.close();
		liste.imprimeListe();
	}
	else
		cout << "Impossible d'ouvrir le fichier" << endl;
	return 0;
}
