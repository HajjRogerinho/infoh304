#include <stdio.h>
#include <string.h>
#define LONGMAX 30
void majuscule(char s[])
{
	int i;
	int diff = 'A' - 'a';
	for ( i=0; i<LONGMAX ; i++ )
	{
//		if ( s[i]>='a' && s[i]<='z' )
//			s[i] = s[i] + diff;
		if ( s[i]>='A' && s[i]<='Z' )
			s[i] = s[i] - diff;
	}
}

int main()
{
	char mot[LONGMAX];
	scanf("%s", mot);
	int i = strlen(mot);
	printf("Le mot %s est de longueur %i\n", mot, i);
	majuscule(mot);
	printf("Le mot en majuscule est %s\n", mot);
	return 0;
}
