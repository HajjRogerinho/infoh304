#include <stdio.h>
#include <string.h>
#define LONGMAX 30
void inverse(char s[])
{
	char temp;
	int i,j;
	for ( i=0, j=strlen(s)-1; i<j; i++, j-- )
	{
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
}

int main()
{
	char mot[LONGMAX];
	scanf("%s", mot);
	int i = strlen(mot);
	printf("Le mot %s est de longueur %i\n", mot, i);
	inverse(mot);
	printf("Le mot inverse est %s\n", mot);
	return 0;
}
