#include <iostream>
#include <string>
#include <map>
using namespace std;

int main(int argc, char *argv[])
{
	pair<map<string,int>::iterator,bool> ret;
	map<string,int> dictionnaire;
	//On essaye d'insérer le mot "test" avec la valeur 10.
	ret=dictionnaire.insert(pair<string,int>("test",10));
	//apres l'insertion, ret vaut (itérateur vers "test",true).
	cout << "ret.first pointe vers la paire: " << ret.first->first << " , " << ret.first->second << endl;
	cout << "ret.second vaut: " << ret.second << endl;
	//On essaye de réinsérer le mot "test" avec la valeur 20.
	ret=dictionnaire.insert(pair<string,int>("test",20));
	//Cette fois, ret vaut (itérateur vers "test",false).
	//Comme "test" existe déjà dans le dictionnaire, sa valeur reste inchangée (10).
	cout << "ret.first pointe vers la paire: " << ret.first->first << " , " << ret.first->second << endl;
	cout << "ret.second vaut: " << ret.second << endl;
}