#include <iostream>
#include <string>
#include <fstream>
#include <map>
using namespace std;

void printMap(const map<string,int> & c, ostream & out = cout )
{
	map<string,int>::const_iterator itr;
	for (itr = c.begin(); itr != c.end(); ++itr )
		out << itr->first << ", " << itr->second << endl;
}

int main(int argc, char *argv[])
{
	map<string,int> dictionnaire;
	ifstream finput (argv[1]);
	if (finput.is_open())
	{
		pair<map<string,int>::iterator,bool> ret;
		string mot;
		while (finput >> mot)
		{
			ret = dictionnaire.insert(pair<string,int>(mot,1));
			if (ret.second == false)
				++(ret.first->second);
		}
		finput.close();
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	ofstream foutput (argv[2]);
	if (foutput.is_open())
	{
		printMap(dictionnaire, foutput);
		foutput.close();
		return 0;
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier de sortie" << endl;
		return 1;
	}
}
