#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

vector<int> values = {1, 2, 5, 10, 20};
int n = values.size();

int ntot(int m){
    if (m < 0){
        return INFINITY;
    } else if (m == 0){
        return 0;
    } else {
        int i = 0;
        while ((i < n - 1) && (values[i + 1] <= m)){
            ++i;
        }
        return ntot(m - values[i]) + 1;
    }
}

int main(int argc, const char *argv[]){
    if (argc == 1){
        cout << "Entrez une valeur" << endl;
    } else {
        int m = atoi(argv[1]);
        int best = ntot(m);
        if (best >= INFINITY){
            cout << "Pas moyen de rendre un montant " << m << "." << endl;
        } else {
            cout << "Pour rendre un montant " << m << ", il faut " << best << " pièces." << endl;
        }
    }
    return 0;
}