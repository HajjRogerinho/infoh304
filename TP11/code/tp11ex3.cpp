#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

vector<int> values = {1, 3, 4, 10};
int n = values.size();

int bestntot(int m){
    if (m < 0){
        return INFINITY;
    } else if (m == 0){
        return 0;
    } else {
        int min = INFINITY;
        for (int i = 0; i < n; i++){
            int current = bestntot(m - values[i]) + 1;
            if (current < min){
                min = current;
            }
        }
        return min;
    }
}

int main(int argc, const char *argv[]){
    if (argc == 1){
        cout << "Entrez une valeur" << endl;
    } else {
        int m = atoi(argv[1]);
        int best = bestntot(m);
        if (best >= INFINITY){
            cout << "Pas moyen de rendre un montant " << m << "." << endl;
        } else {
            cout << "Pour rendre un montant " << m << ", il faut " << best << " pièces." << endl;
        }
    }
    return 0;
}