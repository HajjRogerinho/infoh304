#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

vector<int> values = {1, 2, 5, 10, 20};
int n = values.size();
vector<bool> memoized;
vector<int> ntot;

int bestntot(int m){
    int min = INFINITY;
    if (m < 0){
        return min;
    } else if (memoized[m]){
        return ntot[m];
    } else if (m == 0){
        min = 0;
    } else {
        for (int i = 0; i < n; i++){
            int current = bestntot(m - values[i]) + 1;
            if (current < min){
                min = current;
            }
        }
    }
    ntot[m] = min;
    memoized[m] = true;
    return min;
}

int main(int argc, const char *argv[]){
    if (argc == 1){
        cout << "Entrez une valeur" << endl;
    } else {
        int m = atoi(argv[1]);
        memoized = vector<bool>(m + 1, false);
        ntot = vector<int>(m + 1, 0);
        int best = bestntot(m);
        if (best >= INFINITY){
            cout << "Pas moyen de rendre un montant " << m << "." << endl;
        } else {
            cout << "Pour rendre un montant " << m << ", il faut " << best << " pièces." << endl;
        }
    }
    return 0;
}