#include "noeud.h"

class Liste
{
private:
	int nombreNoeuds;
	Noeud* tete;
public:
	Liste();
	~Liste();
	
	void insere(string chaine);
	void insereTrie(string chaine);
	void supprime();
	void imprimeListe(std::ostream & out=std::cout) const;
	int getNombreNoeuds() const;
	Noeud* getTete();
};
