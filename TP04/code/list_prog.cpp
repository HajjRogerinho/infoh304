#include <iostream>
#include <fstream>
#include "list.h"
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

int main(int argc, char *argv[])
{
	Liste liste;
	ifstream finput (argv[1]);
	if (finput.is_open())
	{
		string mot;
		while (finput >> mot)
			liste.insereTrie(mot);
		finput.close();
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	ofstream foutput (argv[2]);
	if (foutput.is_open())
	{
		liste.imprimeListe(foutput);
		foutput.close();
		return 0;
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier de sortie" << endl;
		return 1;
	}
}
