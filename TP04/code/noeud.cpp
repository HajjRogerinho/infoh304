#include "noeud.h"

Noeud::Noeud()
{
	data = "";
	suivant = NULL;
	compteur = 0;
}

Noeud::Noeud(string chaine)
{
	data = chaine;
	suivant = NULL;
	compteur = 0;
}

void Noeud::setSuivant(Noeud* n)
{
	suivant = n;
}

void Noeud::incrementeCompteur()
{
	compteur++;
}

Noeud* Noeud::getSuivant() const
{
	return suivant;
}
	
string Noeud::getData() const
{
	return data;
}

int Noeud::getCompteur() const
{
	return compteur;
}
