#include "list.h"

Liste::Liste()
{
	nombreNoeuds = 0;
	tete = NULL;
}

Liste::~Liste()
{
	while (tete != NULL)
	{
		supprime();
	}
}

void Liste::insere(string chaine)
{
	Noeud* n = new Noeud(chaine);
	n->setSuivant(tete);
	tete = n;
	nombreNoeuds++;
}

void Liste::insereTrie(string chaine)
{
	if ((tete == NULL) || (tete->getData() > chaine))
	{
		Noeud* n = new Noeud(chaine);
		n->setSuivant(tete);
		tete = n;
		nombreNoeuds++;
	}
	else
	{
		Noeud* temp = tete;
		while ((temp->getSuivant() != NULL) && (temp->getSuivant()->getData() <= chaine))
		{
			temp = temp->getSuivant();
		}
		if (temp->getData() != chaine)
		{
			Noeud* n = new Noeud(chaine);
			n->setSuivant(temp->getSuivant());
			temp->setSuivant(n);
			nombreNoeuds++;
		}
		else
		{
			temp->incrementeCompteur();
		}
	}
}

void Liste::supprime()
{
	if (tete != NULL)
	{
		Noeud* temp = tete;
		tete = tete->getSuivant();
		delete(temp);
		nombreNoeuds--;
	}
	else
		cout << "La liste est deja vide !" << endl;
}

void Liste::imprimeListe(ostream & out) const
{
	Noeud* temp = tete;
	while (temp != NULL)
	{
		out << "Mot : " << temp->getData() << ", nombre d'occurences : " << temp->getCompteur() << endl;
		temp = temp->getSuivant();
	}
}

int Liste::getNombreNoeuds() const
{
	return nombreNoeuds;
}

Noeud* Liste::getTete()
{
	return tete;
}
