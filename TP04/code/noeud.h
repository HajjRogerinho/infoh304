#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::endl;
using std::ostream;

class Noeud
{
private:
	string data;
	Noeud* suivant;
	int compteur;
public:
	Noeud();
	Noeud(string chaine);
	void setSuivant(Noeud* n);
	void incrementeCompteur();
	Noeud* getSuivant() const;
	string getData() const;
	int getCompteur() const;
};
