#include "list.h"
#define LONGMAX 50

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Erreur : ajouter deux arguments\n");
		return 1;
	}
	
	FILE* finput;
	printf("Ouverture du fichier d'entrée : %s\n", argv[1]);
	if ((finput = fopen(argv[1], "r")) == NULL)
	{
		printf("Impossible d'ouvrir ce fichier\n");
		return 2;
	}
	printf("Fichier valide\n");
	
	FILE* foutput;
	printf("Ouverture du fichier de sortie : %s\n", argv[2]);
	if ((foutput = fopen(argv[2], "w")) == NULL)
	{
		printf("Impossible d'ouvrir ce fichier\n");
		return 2;
	}
	printf("Fichier valide\n");
	
	int compte=0;
	char mot[LONGMAX];
	
	noeud* tete = NULL;
	
	while ((fscanf(finput, "%s", mot)) != EOF)
	{
		++compte;
		tete = inserer_noeud(mot, tete);
	}
	fimprimer_liste(foutput, tete);
	fclose(finput);
	fclose(foutput);
	printf("Nombre de mots dans le fichier : %d\n", compte);
	return 0;
}
