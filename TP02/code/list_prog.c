#include "list.h"

int main()
{
	noeud* tete = NULL;
	imprimer_liste(tete);
	
	tete = inserer_noeud("premier",tete);
	imprimer_liste(tete);
	
	tete = inserer_noeud("deuxieme",tete);
	imprimer_liste(tete);
	
	tete = supprimer_tete(tete);
	imprimer_liste(tete);
	
	tete = supprimer_tete(tete);
	imprimer_liste(tete);
	
	tete = supprimer_tete(tete);
	imprimer_liste(tete);
	
	return 0;
}
