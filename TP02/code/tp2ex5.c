#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LONGMAX 50

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		printf("Erreur: ajouter un argument\n");
		return 1;
	}
	printf("Ouverture du fichier : %s\n", argv[1]);
	FILE* fichier;
	if ((fichier = fopen(argv[1], "r+")) == NULL)
	{
		printf("Impossible d'ouvrir ce fichier\n");
		return 2;
	}
	printf("Fichier valide\n");
	
	int compte=0;
	char mot[LONGMAX];
	while ((fscanf(fichier, "%s", mot)) != EOF)
		++compte;
		
	fprintf(fichier, "\nNombre de mots dans le fichier : %d\n", compte);
	fclose(fichier);
	printf("Nombre de mots dans le fichier : %d\n", compte);
	return 0;
}
